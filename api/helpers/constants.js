'use strict;'

module.exports = {
  Sequelize: {
    FKConstraintError: 'SequelizeForeignKeyConstraintError'
  }
};
