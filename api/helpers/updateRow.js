'use strict;'

const { Sequelize } = require('../helpers/constants');
const models = require('../models');
const SendStatus = require('./sendStatus');

module.exports = function (req, res, model, refModel, refModelId, payload, attribute, arr, key) {
  const {
    id
  } = req.swagger.params;

  return models[model].update(payload, {
    where: {
      id: id.value
    }
  }).then(result => {
    if (result[0] === 0) {
      return SendStatus(res, model, id.value);
    }

    const refPayload = key ?
      arr.map(item => ({
        answer: item,
        [refModelId]: id.value,
        correctAnswer: item === key ? 1 : 0
      })) :
      arr.map(item => ({
        [refModelId]: id.value,
        QuestionId: item
      }));

    // Delete previous reference model row
    models[refModel].destroy({
      where: {
        [refModelId]: id.value
      }
    }).catch(err => {
      return res.status(204).send(err);
    });

    // Now update the reference model
    models[refModel].bulkCreate(
      refPayload
    ).catch(err => {
      return res.status(204).send(err);
    });

    res.status(204).send();
  }).catch(err => {
    if (err.name === Sequelize.FKConstraintError) {
      return SendStatus(res, attribute, payload[attribute]);
    }

    res.status(204).send(err);
  });
}
