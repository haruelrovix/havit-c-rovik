'use strict;'

const models = require('../models');
const SendStatus = require('./sendStatus');

module.exports = function (req, res, model, attribute) {
  const {
    id
  } = req.swagger.params;

  return models[model].destroy({
    where: {
      id: id.value
    }
  }).then(result => {
    if (!result) {
      return SendStatus(res, attribute, id.value);
    }

    res.status(204).send();
  }).catch(err => {
    res.status(204).send(err);
  });
}
