'use strict;'

module.exports = function sendStatus(res, message, id) {
  return res.status(404).send({
    message: `${message} not found`,
    id
  });
}
