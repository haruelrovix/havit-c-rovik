'use strict';

const UpdateRow = require('../helpers/updateRow');
const DeleteRow = require('../helpers/deleteRow');
const Tests = require('../models').Tests;
const TestsQuestions = require('../models').TestsQuestions;
const Questions = require('../models').Questions;

// Exports all the functions to perform on the db
module.exports = {
  saveTest,
  getAllTest,
  getTest,
  editTest,
  deleteTest
};

// POST /tests operationId
function saveTest(req, res) {
  const {
    name,
    timeLimit,
    question,
    rightAnswerWeight,
    wrongAnswerWeight
  } = req.body;

  Tests.create({
    name,
    timeLimit,
    rightAnswerWeight,
    wrongAnswerWeight
  }).then(result => {
    const test = result.get({
      plain: true
    });

    const payload = question.map(item => ({
      TestId: test.id,
      QuestionId: item
    }));

    // Now insert questions to test suite
    TestsQuestions.bulkCreate(
      payload
    ).then(() => {
      res.json({
        message: "Test added successfully",
        id: test.id
      });
    }).catch(() => {
      res.json({
        message: "Test added successfully but something's wrong with the question",
        id: test.id
      });
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// GET /tests operationId
function getAllTest(_, res) {
  Tests.findAll({
    include: [
      TestsQuestions
    ]
  }).then(result => {
    const tests = result.map(node => {
      const {
        id,
        name,
        timeLimit,
        rightAnswerWeight,
        wrongAnswerWeight
      } = node.get({
        plain: true
      });

      return {
        id,
        name,
        timeLimit,
        rightAnswerWeight,
        wrongAnswerWeight
      };
    });

    res.json({
      tests
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// GET /tests/{id} operationId
function getTest(req, res) {
  const {
    value: id
  } = req.swagger.params.id;

  Tests.findOne({
    where: {
      id
    },
    include: [{
      model: TestsQuestions,
      include: [
        Questions
      ]
    }]
  }).then(result => {
    if (!result) {
      return res.status(404).send({
        message: "Test not found",
        id: req.swagger.params.id.value
      });
    }

    const {
      id,
      name,
      timeLimit,
      TestsQuestions,
      rightAnswerWeight,
      wrongAnswerWeight
    } = result.get({
      plain: true
    });

    const questions = TestsQuestions.map(({
      Question
    }) => ({
      id: Question.id,
      question: Question.question
    }));

    res.json({
      test: {
        id,
        name,
        timeLimit,
        questions,
        rightAnswerWeight,
        wrongAnswerWeight
      }
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// PUT /tests/{id} operationId
function editTest(req, res) {
  const {
    name,
    timeLimit,
    question,
    rightAnswerWeight,
    wrongAnswerWeight
  } = req.body;

  const payload = {
    name,
    timeLimit,
    rightAnswerWeight,
    wrongAnswerWeight
  };

  return UpdateRow(req, res, 'Tests', 'TestsQuestions', 'TestId', payload, '', question);
}

// DELETE /tests/{id} operationId
function deleteTest(req, res) {
  return DeleteRow(req, res, 'Tests', 'Test');
}
