'use strict';

const sha1 = require('sha1');
const auth = require('../helpers/auth');

const Users = require('../models').Users;
const Roles = require('../models').Roles;

// Exports all the functions to perform on the db
module.exports = {
  login
};

// POST /accounts operationId
function login(req, res) {
  const {
    username,
    password
  } = req.body;

  Users.findOne({
    where: {
      username,
      password: sha1(password)
    },
    include: [
      Roles
    ]
  }).then(result => {
    if (!result) {
      return res.status(403).send({
        message: 'Error: Credentials incorrect'
      });
    }

    const {
      id,
      username,
      Role: {
        role
      }
    } = result.get({
      plain: true
    });

    return res.json({
      token: auth.issueToken(id, username, role)
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}
