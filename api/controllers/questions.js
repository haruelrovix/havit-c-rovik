'use strict';

const Constants = require('../helpers/constants');
const UpdateRow = require('../helpers/updateRow');
const DeleteRow = require('../helpers/deleteRow');
const Questions = require('../models').Questions;
const Subjects = require('../models').Subjects;
const Levels = require('../models').Levels;
const Answers = require('../models').Answers;

// Exports all the functions to perform on the db
module.exports = {
  save,
  getAll,
  edit,
  get,
  deleteQuestion
};

// POST /questions operationId
function save(req, res) {
  const {
    question,
    subject,
    level,
    choice,
    answer
  } = req.body;

  if (!choice.includes(answer)) {
    return res.status(404).send({
      message: 'Answer should exist inside multiple choice',
      id: -1
    });
  }

  Questions.create({
    question,
    SubjectId: subject,
    LevelId: level
  }).then(result => {
    const question = result.get({
      plain: true
    });

    const payload = choice.map(item => ({
      answer: item,
      QuestionId: question.id,
      correctAnswer: item === answer ? 1 : 0
    }));

    // Now insert the multiple choice
    Answers.bulkCreate(
      payload
    ).catch(err => {
      return res.status(204).send(err);
    });

    res.json({
      message: "Question added successfully",
      id: question.id
    });
  }).catch(err => {
    if (err.name === Constants.Sequelize.FKConstraintError) {
      return res.status(404).send({
        message: 'Level / Subject not found',
        id: -1
      });
    }

    res.status(204).send(err);
  });
}

// GET /questions operationId
function getAll(_, res) {
  Questions.findAll({
    include: [
      Subjects,
      Levels,
      Answers
    ]
  }).then(result => {
    const questions = result.map(node => {
      const {
        id,
        question,
        Subject: {
          subject
        },
        Level: {
          level
        }
      } = node.get({
        plain: true
      });

      return {
        id,
        question,
        subject,
        level
      };
    });

    res.json({
      questions
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// PUT /questions/{id} operationId
function edit(req, res) {
  const {
    question,
    subject,
    level,
    choice,
    answer
  } = req.body;

  if (!choice.includes(answer)) {
    return res.status(404).send({
      message: 'Answer should exist inside multiple choice',
      id: -1
    });
  }

  const payload = {
    question,
    SubjectId: subject,
    LevelId: level
  };

  return UpdateRow(req, res, 'Questions', 'Answers', 'QuestionId', payload, 'Subject / Level', choice, answer);
}

// GET /questions/{id} operationId
function get(req, res) {
  const {
    value: id
  } = req.swagger.params.id;

  Questions.findOne({
    where: {
      id
    },
    include: [
      Subjects,
      Levels,
      Answers
    ]
  }).then(result => {
    if (!result) {
      return res.status(404).send({
        message: "Question not found",
        id: req.swagger.params.id.value
      });
    }

    const {
      id,
      question,
      Subject: {
        subject
      },
      Level: {
        level
      },
      Answers
    } = result.get({
      plain: true
    });

    // Multiple choice and the answer
    let correctAnswer;
    const choice = Answers.map(({
      id,
      answer,
      correctAnswer: correct
    }) => {
      if (correct) {
        correctAnswer = id;
      }

      return {
        id,
        answer
      };
    });

    res.json({
      question: {
        id,
        question,
        subject,
        level,
        choice,
        answer: correctAnswer
      }
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// DELETE /questions/{id} operationId
function deleteQuestion(req, res) {
  return DeleteRow(req, res, 'Questions', 'Question');
}
