'use strict';

const Answers = require('../models').Answers;
const Questions = require('../models').Questions;
const Tests = require('../models').Tests;
const TestsQuestions = require('../models').TestsQuestions;
const TryOuts = require('../models').TryOuts;
const Users = require('../models').Users;

// Exports all the functions to perform on the db
module.exports = {
  saveTryOut,
  submitTryOut,
  getTryOutParticipants
};

// POST /tryout operationId
function saveTryOut(req, res) {
  const {
    testId: id
  } = req.body;

  Tests.findOne({
    where: {
      id
    },
    include: [{
      model: TestsQuestions,
      include: [{
        model: Questions,
        include: [{
          model: Answers
        }]
      }]
    }]
  }).then(result => {
    if (!result) {
      return res.status(404).send({
        message: "Test not found",
        id
      });
    }

    TryOuts.findOne({
      where: {
        UserId: req.auth.userId,
        TestId: id
      }
    }).then(exist => {
      if (exist) {
        return res.status(404).send({
          message: "You have participated on this Try Out",
          id
        });
      }

      const {
        TestsQuestions,
        timeLimit
      } = result.get({
        plain: true
      });

      const startAt = Date.now();

      TryOuts.create({
        UserId: req.auth.userId,
        TestId: id,
        notAnswered: TestsQuestions.length,
        startAt
      }).then(r => {
        const tryOut = r.get({
          plain: true
        });

        const questions = TestsQuestions.map(({
          QuestionId,
          Question
        }) => {
          const answers = Question.Answers.map(({
            id,
            answer
          }) => ({
            id,
            answer
          }));

          return {
            id: QuestionId,
            question: Question.question,
            multipleChoice: answers
          };
        });

        res.json({
          message: "Try Out added successfully",
          id: tryOut.id,
          questions,
          startAt: (new Date(startAt)).toISOString(),
          timeLimit
        });
      }).catch(err => {
        res.status(204).send(err);
      });
    }).catch(err => {
      res.status(204).send(err);
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// PUT /tryout/{id}/submit operationId
function submitTryOut(req, res) {
  const {
    value: id
  } = req.swagger.params.id;

  let testId;

  TryOuts.findOne({
    where: {
      id
    },
    include: [{
      model: Tests,
      include: [{
        model: TestsQuestions,
        include: [{
          model: Questions,
          include: [{
            model: Answers
          }]
        }]
      }]
    }]
  }).then(result => {
    if (!result) {
      return res.status(404).send({
        message: "Try Out not found",
        id
      });
    }

    const tryOut = result.get({
      plain: true
    });

    if (req.auth.userId !== tryOut.UserId) {
      return res.json({
        message: "Try Out belongs to other participant",
        id
      });
    }

    if (Date.now().valueOf() > (tryOut.startAt.valueOf() + tryOut.Test.timeLimit)) {
      return res.json({
        message: "Time limit for Try Out has been exceeded",
        id
      });
    }

    testId = tryOut.TestId;

    const {
      submission
    } = req.body;
    const {
      TestsQuestions,
      rightAnswerWeight,
      wrongAnswerWeight
    } = tryOut.Test;

    let correctAnswer = 0;
    let wrongAnswer = 0;

    submission.forEach(({
      questionId,
      answerId
    }) => {
      const testQuestion = TestsQuestions.find(q => q.QuestionId === questionId);

      if (testQuestion) {
        const keyAnswer = testQuestion.Question.Answers.find(q => q.correctAnswer === true);

        if (keyAnswer) {
          if (keyAnswer.id === answerId) {
            correctAnswer++;
          } else {
            wrongAnswer++;
          }
        }
      }
    });

    const notAnswered = TestsQuestions.length - correctAnswer - wrongAnswer;
    const score = rightAnswerWeight * correctAnswer + wrongAnswerWeight * wrongAnswer;
    const submittedAt = Date.now();

    const payload = {
      correctAnswer,
      wrongAnswer,
      notAnswered,
      score,
      submittedAt
    };

    TryOuts.update(payload, {
      where: {
        id
      }
    }).then(() => {
      TryOuts.findAll({
        where: {
          TestId: testId
        },
        include: [
          Users,
          Tests
        ],
        order: [
          ['score', 'DESC'],
          ['submittedAt', 'ASC'],
          ['notAnswered', 'ASC'],
          [Users, 'username', 'ASC']
        ]
      }).then(result => {
        const participants = result.map(node => {
          const {
            User
          } = node.get({
            plain: true
          });
    
          return {
            name: User.username
          };
        });

        const ranking = participants.findIndex(participant => participant.name === req.auth.username) + 1;

        res.json({
          message: "Answer for Try Out has been submitted successfully",
          id,
          statistic: {
            percentage: wrongAnswer === 0 ? 100 : (correctAnswer / wrongAnswer * 100),
            notAnswered,
            correctAnswer,
            wrongAnswer,
            score,
            duration: submittedAt.valueOf() - tryOut.startAt.valueOf(),
            ranking
          }
        });
      })
    }).catch(err => {
      res.status(204).send(err);
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}

// GET /tryout/{testId}/participants operationId
function getTryOutParticipants(req, res) {
  const {
    value: testId
  } = req.swagger.params.testId;

  TryOuts.findAll({
    where: {
      TestId: testId
    },
    include: [
      Users,
      Tests
    ],
    order: [
      ['score', 'DESC'],
      ['submittedAt', 'DESC'],
      ['notAnswered', 'ASC'],
      [Users, 'username', 'ASC']
    ]
  }).then(result => {
    if (!result || result.length === 0) {
      return res.status(404).send({
        message: "No participants for the Test",
        id: testId
      });
    }

    const {
      Test: {
        name: testName,
        timeLimit
      }
    } = result[0].get({
      plain: true
    });

    const participants = result.map(node => {
      const {
        User,
        notAnswered,
        correctAnswer,
        wrongAnswer,
        score,
        startAt,
        submittedAt
      } = node.get({
        plain: true
      });

      return {
        name: User.username,
        percentage: wrongAnswer === 0 ? 100 : (correctAnswer / wrongAnswer * 100),
        notAnswered,
        correctAnswer,
        wrongAnswer,
        score,
        duration: submittedAt.valueOf() - startAt.valueOf()
      };
    });

    res.json({
      testId,
      testName,
      timeLimit,
      participants
    });
  }).catch(err => {
    res.status(204).send(err);
  });
}
