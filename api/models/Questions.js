module.exports = function (sequelize, DataTypes) {
  const Questions = sequelize.define('Questions', {
    question: {
      type: DataTypes.STRING
    },
    SubjectId: {
      type: DataTypes.INTEGER,
      field: 'subject_id'
    },
    LevelId: {
      type: DataTypes.INTEGER,
      field: 'level_id',
    }
  }, {
    tableName: 'questions',
    timestamps: false
  });

  Questions.associate = function (models) {
    Questions.belongsTo(models.Subjects);
    Questions.belongsTo(models.Levels);
    Questions.hasMany(models.Answers, {
      onDelete: 'CASCADE',
      hooks: true
    });
    Questions.hasMany(models.TestsQuestions, {
      onDelete: 'CASCADE',
      hooks: true
    });
  };

  return Questions;
};
