module.exports = function (sequelize, DataTypes) {
  const Answers = sequelize.define('Answers', {
    answer: {
      type: DataTypes.STRING
    },
    correctAnswer: {
      type: DataTypes.BOOLEAN,
      field: 'correct_answer',
    },
    QuestionId: {
      type: DataTypes.INTEGER,
      field: 'question_id',
    }
  }, {
    tableName: 'answers',
    timestamps: false
  });

  Answers.associate = function (models) {
    Answers.belongsTo(models.Questions);
  };

  return Answers;
};
