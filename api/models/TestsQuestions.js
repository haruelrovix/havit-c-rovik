module.exports = function (sequelize, DataTypes) {
  const TestsQuestions = sequelize.define('TestsQuestions', {
    TestId: {
      type: DataTypes.INTEGER,
      field: 'test_id',
      primaryKey: true
    },
    QuestionId: {
      type: DataTypes.INTEGER,
      field: 'question_id',
      primaryKey: true
    }
  }, {
    tableName: 'tests_questions',
    timestamps: false
  });

  TestsQuestions.associate = function (models) {
    TestsQuestions.belongsTo(models.Tests);
    TestsQuestions.belongsTo(models.Questions);
  };

  return TestsQuestions;
};
