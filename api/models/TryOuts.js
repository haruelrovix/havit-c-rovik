module.exports = function (sequelize, DataTypes) {
  const TryOuts = sequelize.define('TryOuts', {
    UserId: {
      type: DataTypes.INTEGER,
      field: 'user_id'
    },
    TestId: {
      type: DataTypes.INTEGER,
      field: 'test_id',
    },
    correctAnswer: {
      type: DataTypes.INTEGER,
      field: 'correct_answer'
    },
    wrongAnswer: {
      type: DataTypes.INTEGER,
      field: 'wrong_answer'
    },
    notAnswered: {
      type: DataTypes.INTEGER,
      field: 'not_answered'
    },
    score: {
      type: DataTypes.INTEGER
    },
    startAt: {
      type: DataTypes.DATE,
      field: 'start_at'
    },
    submittedAt: {
      type: DataTypes.DATE,
      field: 'submitted_at'
    }
  }, {
    tableName: 'tryouts',
    timestamps: false
  });

  TryOuts.associate = function (models) {
    TryOuts.belongsTo(models.Tests, {
      onDelete: 'CASCADE',
      hooks: true
    });
    TryOuts.belongsTo(models.Users);
  };

  return TryOuts;
};
