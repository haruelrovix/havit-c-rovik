module.exports = function (sequelize, DataTypes) {
  const Roles = sequelize.define('Roles', {
    role: {
      type: DataTypes.STRING
    }
  }, {
    tableName: 'roles',
    timestamps: false
  }, );

  Roles.associate = function (models) {
    Roles.hasMany(models.Users);
  };

  return Roles;
};