module.exports = function (sequelize, DataTypes) {
  const Subjects = sequelize.define('Subjects', {
    subject: {
      type: DataTypes.STRING
    }
  }, {
    tableName: 'subjects',
    timestamps: false
  });

  Subjects.associate = function (models) {
    Subjects.hasMany(models.Questions);
  };

  return Subjects;
};
