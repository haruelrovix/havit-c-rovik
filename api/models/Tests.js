module.exports = function (sequelize, DataTypes) {
  const Tests = sequelize.define('Tests', {
    name: {
      type: DataTypes.STRING
    },
    timeLimit: {
      type: DataTypes.INTEGER,
      field: 'time_limit'
    },
    rightAnswerWeight: {
      type: DataTypes.INTEGER,
      field: 'right_answer_weight'
    },
    wrongAnswerWeight: {
      type: DataTypes.INTEGER,
      field: 'wrong_answer_weight'
    }
  }, {
    tableName: 'tests',
    timestamps: false
  });

  Tests.associate = function (models) {
    Tests.hasMany(models.TestsQuestions);
    Tests.hasMany(models.TryOuts);
  };

  return Tests;
};
