module.exports = function (sequelize, DataTypes) {
  const Levels = sequelize.define('Levels', {
    level: {
      type: DataTypes.STRING
    }
  }, {
    tableName: 'levels',
    timestamps: false
  });

  Levels.associate = function (models) {
    Levels.hasMany(models.Questions);
  };

  return Levels;
};
