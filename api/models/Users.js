module.exports = function (sequelize, DataTypes) {
  const Users = sequelize.define('Users', {
    username: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING
    },
    RoleId: {
      type: DataTypes.INTEGER,
      field: 'role_id'
    }
  }, {
    tableName: 'users',
    timestamps: false
  });

  Users.associate = function (models) {
    Users.belongsTo(models.Roles);
    Users.hasMany(models.TryOuts);
  }

  return Users;
};
