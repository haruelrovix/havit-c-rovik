'use strict;'

module.exports = {
  database: {
    name: 'tryout',
    dialect: 'sqlite',
    storage: 'tryout.db'
  },
  auth: {
    secret: "it'S3c12eT,d0od!",
    iss: 'surat@havit.web.id',
    expiresIn: '3h'
  }
};
