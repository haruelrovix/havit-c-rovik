
# Try Out REST API

Try Out Backend Service APIs for students and admin. Student can use the API to participate multiple tryout tests. Admin can create a test and questions (multiple choice question) and see list of participants test data in a Test.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Prerequisites

What things you need to have:

1. [Node](https://nodejs.org/en/)

        $ node -v
        v9.11.1

2. [SQLite](https://www.sqlite.org/index.html)

        $ sqlite3 -version
        3.24.0 2018-06-04 19:24:41 c7ee0833225bfd8c5ec2f9bf62b97c4e04d03bd9566366d5221ac8fb199a87ca

3. [Yarn](https://yarnpkg.com/en/)

        $ yarn -v
        1.9.4


### Installing

A step by step how to get a **development** env running.

1. Get the code and `cd` into it

        $ git clone https://haruelrovix@bitbucket.org/haruelrovix/havit-c-rovik.git && cd havit-c-rovik
        ...
        Resolving deltas: 100% (132/132), done.
        Checking connectivity... done.

2. `master` is for `Production` cloud. Change to `develop` branch.

        $ git checkout develop && git status
        ...
        On branch develop
        Your branch is up-to-date with 'origin/develop'.
        nothing to commit, working directory clean

3. Install package dependencies

        $ yarn
        ...
        [3/4] Linking dependencies...
        [4/4] Building fresh packages...
        Done in 105.90s.

4. Create and import `tryout` database

        $ ./scripts/init-db
        ...
        Pouring something into it...
        tryout.db is ready to rock

     _If you found error on this step_, make sure you have executable rights for `init-db` script.

        $ ls -l scripts
        ...
        -rwxrwxrwx 1 root root 353 Aug 13 12:27 scripts/init-db

5. Run the app

        $ yarn start
        ...
        Executing (default): PRAGMA INDEX_LIST(`tryouts`)
        Connection has been established successfully.

6. Open your browser, hit this URL.

    [http://localhost:10010/docs/](http://localhost:10010/docs/) 

    ![Try Out API on localhost](https://i.imgur.com/lzecfYu.jpg)

7. Happy API testing! 🎉


## Testing the API

Basically, you need a `Bearer {token}` to access the endpoint.

### Get the token

Hit `login` endpoint to get the token.

       curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
         "username": "provide@valid.email",
         "password": "AndAlsoThePassword"
       }' 'https://try-out-api.appspot.com/api/login'

### Copy the token

Copy the value from `token` payload.

       {"token":"you-should-copy-value-here"}

### The `REST` is easy

The Authorization format is:

       Bearer {token}

Try to hit `questions` endpoint.

       curl -X GET --header 'Accept: application/json' --header 'Authorization: Bearer {token}' 'https://try-out-api.appspot.com/api/questions'

![Try Out Questions endpoint response](https://i.imgur.com/UPvkQnk.jpg)

Others API Testing documentation can be found under `docs` folder on this repository.


## Deployment

Hosted on [Google Cloud Platform](https://cloud.google.com/), see it **Live** here:

✔️ **https://try-out-api.appspot.com/docs/**


## Built With

* [Swagger](https://swagger.io/) - The Best APIs are Built with Swagger Tools
* [Node.js](https://nodejs.org/en/) - JavaScript runtime built on [Chrome's V8 JavaScript engine](https://developers.google.com/v8/)
* [Sequelize](https://github.com/sequelize/sequelize) - An easy-to-use multi SQL dialect ORM for Node.js


## Author

* [haruelrovix](https://github.com/haruelrovix)
