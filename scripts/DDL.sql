--
-- File generated with SQLiteStudio v3.1.1 on Sat Aug 11 06:36:59 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: answers
DROP TABLE IF EXISTS answers;
CREATE TABLE answers (id INTEGER PRIMARY KEY AUTOINCREMENT, answer VARCHAR NOT NULL, question_id INTEGER REFERENCES questions (id) ON DELETE CASCADE, correct_answer INTEGER DEFAULT (0));

-- Table: levels
DROP TABLE IF EXISTS levels;
CREATE TABLE levels (id INTEGER PRIMARY KEY AUTOINCREMENT, level VARCHAR (10) NOT NULL);

-- Table: questions
DROP TABLE IF EXISTS questions;
CREATE TABLE questions (id INTEGER PRIMARY KEY AUTOINCREMENT, question VARCHAR NOT NULL, subject_id INTEGER REFERENCES subjects (id), level_id INTEGER REFERENCES levels (id));

-- Table: roles
DROP TABLE IF EXISTS roles;
CREATE TABLE roles (id INTEGER PRIMARY KEY AUTOINCREMENT, role VARCHAR (10) NOT NULL);

-- Table: subjects
DROP TABLE IF EXISTS subjects;
CREATE TABLE subjects (id INTEGER PRIMARY KEY AUTOINCREMENT, subject VARCHAR (30) NOT NULL);

-- Table: tests
DROP TABLE IF EXISTS tests;
CREATE TABLE tests (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR NOT NULL, time_limit INTEGER, right_answer_weight INTEGER, wrong_answer_weight INTEGER);

-- Table: tests_questions
DROP TABLE IF EXISTS tests_questions;
CREATE TABLE tests_questions (test_id INTEGER REFERENCES tests (id) ON DELETE CASCADE NOT NULL, question_id INTEGER REFERENCES questions (id) ON DELETE CASCADE NOT NULL);

-- Table: tryouts
DROP TABLE IF EXISTS tryouts;
CREATE TABLE tryouts (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER REFERENCES users (id) NOT NULL, test_id INTEGER REFERENCES tests (id) ON DELETE CASCADE NOT NULL, correct_answer INTEGER, not_answered INTEGER, wrong_answer INTEGER, score INTEGER, start_at DATETIME, submitted_at DATETIME);

-- Table: users
DROP TABLE IF EXISTS users;
CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, username VARCHAR (50) NOT NULL, password VARCHAR (50) NOT NULL, role_id INTEGER REFERENCES roles (id));

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
