--
-- File generated with SQLiteStudio v3.1.1 on Sat Aug 11 06:36:59 2018
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: answers
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (6, 'x2 = 2x - 10', 2, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (7, 'x + 2 = 5', 2, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (8, '2x + 3 = 4x - 5', 2, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (9, 'x3 + 2x2 + x - 1 = 0', 2, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (10, 'y = x + 7', 2, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (31, '0', 7, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (32, '1', 7, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (33, '2', 7, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (34, '3', 7, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (35, '5', 7, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (36, '2', 8, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (37, '7', 8, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (38, '9', 8, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (39, '10', 8, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (40, '{Faktor dari 4} dan {a, b, c, d}', 9, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (41, '{Bilangan prima kurang dari 6} dan {a, b, c}', 9, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (42, '{Bilangan cacah kelipatan 3 kurang dari 9} dan {p, q, r}', 9, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (43, '{Faktor dari 10} dan {q, r, s}', 9, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (44, 'Jumlah sudut-sudut dalam berseberangan 180°', 10, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (45, 'Sudut-sudut bertolak belakang tidak sama besar', 10, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (46, 'Sudut-sudut luar berseberangan sama besar', 10, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (47, 'Jumlah dua sudut dalam sepihak 360°', 10, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (52, '60 cm2', 12, 1);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (53, '65 cm2', 12, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (54, '120 cm2', 12, 0);
INSERT INTO answers (id, answer, question_id, correct_answer) VALUES (55, '130 cm2', 12, 0);

-- Table: levels
INSERT INTO levels (id, level) VALUES (1, 'SMP1');
INSERT INTO levels (id, level) VALUES (2, 'SMP2');
INSERT INTO levels (id, level) VALUES (3, 'SMA1');
INSERT INTO levels (id, level) VALUES (4, 'SMA2');

-- Table: questions
INSERT INTO questions (id, question, subject_id, level_id) VALUES (2, 'Berikut ini yang merupakan persamaan kuadrat adalah...', 3, 1);
INSERT INTO questions (id, question, subject_id, level_id) VALUES (7, 'Nilai dari bentuk 5log 3 + 5log 4 + 5log 2 - 5log 6 adalah...', 3, 1);
INSERT INTO questions (id, question, subject_id, level_id) VALUES (8, 'Diketahui P = {a, b, c, d, e}. Banyaknya himpunan bagian dari P yang mempunyai tiga anggota adalah...', 3, 1);
INSERT INTO questions (id, question, subject_id, level_id) VALUES (9, 'Diantara empat pasangan himpunan di bawah ini yang merupakan pasangan yang ekuivalen adalah...', 3, 1);
INSERT INTO questions (id, question, subject_id, level_id) VALUES (10, 'Pernyataan berikut yang benar adalah...', 3, 1);
INSERT INTO questions (id, question, subject_id, level_id) VALUES (12, 'Luas segitiga samakaki dengan alas 10 cm dan keliling 36 cm adalah...', 3, 1);

-- Table: roles
INSERT INTO roles (id, role) VALUES (1, 'Student');
INSERT INTO roles (id, role) VALUES (2, 'Admin');

-- Table: subjects
INSERT INTO subjects (id, subject) VALUES (1, 'Bahasa Indonesia');
INSERT INTO subjects (id, subject) VALUES (2, 'Ekonomi');
INSERT INTO subjects (id, subject) VALUES (3, 'Matematika');

-- Table: tests
INSERT INTO tests (id, name, time_limit, right_answer_weight, wrong_answer_weight) VALUES (1, 'Matematika untuk SMA Kelas 1', 900000, 4, -2);
INSERT INTO tests (id, name, time_limit, right_answer_weight, wrong_answer_weight) VALUES (2, 'Matematika untuk SMA Kelas 2', 900000, 5, -1);
INSERT INTO tests (id, name, time_limit, right_answer_weight, wrong_answer_weight) VALUES (3, 'Matematika untuk SMA Kelas 3', 900000, 4, 0);
INSERT INTO tests (id, name, time_limit, right_answer_weight, wrong_answer_weight) VALUES (10, 'Latihan Soal UKK Kelas 8 SMP', 900000, 4, -2);
INSERT INTO tests (id, name, time_limit, right_answer_weight, wrong_answer_weight) VALUES (11, 'Latihan Soal UKK Kelas 7 SMP', 900000, 3, 0);

-- Table: tests_questions
INSERT INTO tests_questions (test_id, question_id) VALUES (8, 2);
INSERT INTO tests_questions (test_id, question_id) VALUES (8, 5);
INSERT INTO tests_questions (test_id, question_id) VALUES (8, 7);
INSERT INTO tests_questions (test_id, question_id) VALUES (10, 10);
INSERT INTO tests_questions (test_id, question_id) VALUES (10, 12);
INSERT INTO tests_questions (test_id, question_id) VALUES (10, 9);
INSERT INTO tests_questions (test_id, question_id) VALUES (10, 8);
INSERT INTO tests_questions (test_id, question_id) VALUES (11, 10);
INSERT INTO tests_questions (test_id, question_id) VALUES (11, 9);
INSERT INTO tests_questions (test_id, question_id) VALUES (11, 12);
INSERT INTO tests_questions (test_id, question_id) VALUES (11, 9);
INSERT INTO tests_questions (test_id, question_id) VALUES (11, 8);

-- Table: tryouts
INSERT INTO tryouts (id, user_id, test_id, correct_answer, not_answered, wrong_answer, score, start_at, submitted_at) VALUES (13, 3, 10, 1, 2, 1, 3, '2018-08-12 13:01:51.782 +00:00', '2018-08-12 13:03:23.012 +00:00');
INSERT INTO tryouts (id, user_id, test_id, correct_answer, not_answered, wrong_answer, score, start_at, submitted_at) VALUES (15, 2, 11, 1, 1, 2, 3, '2018-08-12 21:37:01.420 +00:00', '2018-08-12 21:37:10.898 +00:00');
INSERT INTO tryouts (id, user_id, test_id, correct_answer, not_answered, wrong_answer, score, start_at, submitted_at) VALUES (18, 4, 11, 4, 0, 0, 12, '2018-08-12 23:34:43.723 +00:00', '2018-08-12 23:47:24.122 +00:00');
INSERT INTO tryouts (id, user_id, test_id, correct_answer, not_answered, wrong_answer, score, start_at, submitted_at) VALUES (20, 3, 11, 4, 0, 0, 12, '2018-08-13 01:06:37.507 +00:00', '2018-08-13 01:10:00.981 +00:00');

-- Table: users
INSERT INTO users (id, username, password, role_id) VALUES (1, 'admin@mail.com', 'b63c5d259ee17b7948c2ca0d9688b75e4279853c', 2);
INSERT INTO users (id, username, password, role_id) VALUES (2, 'student3@mail.com', 'b63c5d259ee17b7948c2ca0d9688b75e4279853c', 1);
INSERT INTO users (id, username, password, role_id) VALUES (3, 'student2@mail.com', 'b63c5d259ee17b7948c2ca0d9688b75e4279853c', 1);
INSERT INTO users (id, username, password, role_id) VALUES (4, 'student1@mail.com', 'b63c5d259ee17b7948c2ca0d9688b75e4279853c', 1);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
